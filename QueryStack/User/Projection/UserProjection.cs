namespace QueryStack.User
{
    public class UserProjection
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }   
    }
}