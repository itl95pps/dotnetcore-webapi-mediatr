using System.Collections.Generic;
using System.Linq;
using Context;
using MediatR;

namespace QueryStack.User
{
    public class UserQuery : IRequest<List<UserProjection>>
    {
    }

    public class UserQueryHandler : IRequestHandler<UserQuery, List<UserProjection>>
    {
        List<UserProjection> IRequestHandler<UserQuery, List<UserProjection>>.Handle(UserQuery message)
        {
            var projection = new List<UserProjection>();
            using (var context = new ApplicationContext())
            {
                projection = 
                context.User.Select(x=> new UserProjection{
                    Id = x.Id,
                    Name = x.Name,
                    Password = x.Password
                }).ToList();
            }
            return projection;
        }
    }

}