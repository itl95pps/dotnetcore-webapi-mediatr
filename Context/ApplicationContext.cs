﻿using System;
using Microsoft.EntityFrameworkCore;
using Model;

namespace Context
{
    public class ApplicationContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=localhost;Database=dotnetcore;User Id=xxxxx;Password=xxxxx;");
        }

        public ApplicationContext()
        {
            Database.EnsureCreated();   
        }

        public DbSet<User> User { get; set; }
        public DbSet<Post> Post { get; set; }
    }
}