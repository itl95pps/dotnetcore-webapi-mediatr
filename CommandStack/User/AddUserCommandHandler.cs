using Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
 

namespace CommandStack.User
{
    public class AddUserCommand : IRequest
    {
        public AddUserCommand(string name, string password, int age)
        {
            Name = name;
            Password = password;
            Age = age;
        }

        public string Name { get; set; }
        public string Password { get; set; }
        public int Age { get; set; }
    }

    public class AddUserCommandHandler : IRequestHandler<AddUserCommand>
    {
        
        public void Handle(AddUserCommand message)
        {
            using (var context = new ApplicationContext())
            {
                context.User.Add(new Model.User{
                    Name = message.Name,
                    Password = message.Password,
                    Age = message.Age
                });
                context.SaveChanges();
            }
        }
    }
}