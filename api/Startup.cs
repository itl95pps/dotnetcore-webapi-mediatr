﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using CommandStack.User;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using QueryStack.User;

namespace api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            services.AddScoped<IMediator, Mediator>();
            services.AddTransient<SingleInstanceFactory>(sp => t => sp.GetService(t));
            services.AddTransient<MultiInstanceFactory>(sp => t => sp.GetServices(t));
                        
            services.AddMediatorHandlers(typeof(Startup).GetTypeInfo().Assembly);

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }

     public static class MediatorExtensions
    {
        public static IServiceCollection AddMediatorHandlers(this IServiceCollection services, Assembly assembly)
        {
            var listTypes = new List<TypeInfo>();
            listTypes.AddRange(GetAssembliesQuery());
            listTypes.AddRange(GetAssembliesCommand());

            RegisterHandlers(services, listTypes);
            return services;
        }

        private static void RegisterHandlers(IServiceCollection services, IEnumerable<TypeInfo> classTypes)
        {
            foreach (var type in classTypes)
            {
                var interfaces = type.ImplementedInterfaces.Select(i => i.GetTypeInfo());

                foreach (var handlerType in interfaces.Where
                    (i => i.IsGenericType && 
                        (
                            (i.GetGenericTypeDefinition() == typeof(IRequestHandler<,>) ||
                            i.GetGenericTypeDefinition() == typeof(IRequestHandler<>))
                            ||
                            (i.GetGenericTypeDefinition() == typeof(IAsyncRequestHandler<,>) ||
                            i.GetGenericTypeDefinition() == typeof(IAsyncRequestHandler<>))
                        )
                    )
                )
                {
                    services.AddTransient(handlerType.AsType(), type.AsType());
                }
            }
        }

        private static IEnumerable<TypeInfo> GetAssembliesQuery()
        {
            return typeof(UserQueryHandler).GetTypeInfo().Assembly.GetExportedTypes().Select(t => t.GetTypeInfo()).Where(t => t.IsClass && !t.IsAbstract);
        }
        private static IEnumerable<TypeInfo> GetAssembliesCommand()
        {
            return typeof(AddUserCommandHandler).GetTypeInfo().Assembly.GetExportedTypes().Select(t => t.GetTypeInfo()).Where(t => t.IsClass && !t.IsAbstract);
        }
    }
}
