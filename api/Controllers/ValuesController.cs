﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommandStack.User;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Model;
using QueryStack.User;

namespace api.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        private IMediator mediator;
        public ValuesController(IMediator mediator)
        {
            this.mediator = mediator;
        }
        
        // GET api/values
        [HttpGet]
        public IEnumerable<UserProjection> Get()
        {
            mediator.Send(new AddUserCommand(Guid.NewGuid().ToString(), Guid.NewGuid().ToString(), new Random().Next()));
            return mediator.Send(new UserQuery()).Result.ToArray();
            
            // return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
